import { LightningElement, api } from 'lwc';

export default class TimeSheetCreateItem extends LightningElement {
    @api selectedts
    @api nnn
    dayHours = {
        id: null,
        frm: null,
        to: null,
        selectedDate: null,
        comment: null
    }

    connectedCallback() {
        if (this.selectedts) {
            this.dayHours = { ...this.dayHours, selectedDate: this.selectedts.value, id: this.selectedts.id }
        }
    }

    handleFrom(event) {
        event.preventDefault();
        const frm = event.detail.value;
        this.dayHours = { ...this.dayHours, frm }
        this.setCurrentDateHours()
    }

    handleTo(event) {
        event.preventDefault();
        const to = event.detail.value;
        this.dayHours = { ...this.dayHours, to }
        this.setCurrentDateHours()
    }

    handleComment(event) {
        event.preventDefault()
        const comment = event.detail.value;
        this.dayHours = { ...this.dayHours, comment }
        this.setCurrentDateHours()
    }

    setCurrentDateHours() {
        this.dispatchEvent(new CustomEvent('currentdatedata', { detail: this.dayHours }));
    }
}