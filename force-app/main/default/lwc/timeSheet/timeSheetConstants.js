export const defaultcolumns = [
  { label: "Employee", fieldName: "employee" },
  { label: "From", fieldName: "Time_From__c" },
  { label: "To", fieldName: "Time_To__c" },
  { label: "For Date", fieldName: "fordate" },
  { label: "Comments", fieldName: "Comments__c" },
  { label: "Status", fieldName: "Status__c" },
  { label: "Date Submitted", fieldName: "Date_Submitted__c", type: "date" }
];

export const managerColumns = [
  { label: "Employee", fieldName: "employee" },
  { label: "From", fieldName: "Time_From__c", editable: true },
  { label: "To", fieldName: "Time_To__c", editable: true },
  { label: "For Date", fieldName: "fordate" },
  { label: "Comments", fieldName: "Comments__c" },
  { label: "Status", fieldName: "Status__c" },
  { label: "Date Submitted", fieldName: "Date_Submitted__c", type: "date" },
  {
    label: "Action",
    type: "action",
    typeAttributes: {
      rowActions: [
        { label: "Approve", name: "Approve" },
        { label: "Reject", name: "Reject" }
      ]
    }
  }
];

// const actions = [
//     { label: 'Approve', name: 'Approve' },
//     { label: 'Reject', name: 'Reject' },
// ];
