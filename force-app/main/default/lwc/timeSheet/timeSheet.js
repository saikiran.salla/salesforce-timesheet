import { LightningElement, wire } from 'lwc';
import isManager from '@salesforce/apex/TimeSheet.isManager';
import createTimeSheet from '@salesforce/apex/TimeSheet.createTimeSheet';
// import modifyTimeSheet from '@salesforce/apex/TimeSheet.modifyTimeSheet';
import alreadySubmitted from '@salesforce/apex/TimeSheet.alreadySubmitted';
import getTimeSheets from '@salesforce/apex/TimeSheet.getTimeSheets';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { updateRecord } from 'lightning/uiRecordApi';
import STATUS_FIELD from '@salesforce/schema/Timesheet__c.Status__c';
import ID_FIELD from '@salesforce/schema/Timesheet__c.Id';
import { refreshApex } from '@salesforce/apex';
import {defaultcolumns, managerColumns} from './timeSheetConstants'

const actions = [
    { label: 'Approve', name: 'Approve' },
    { label: 'Reject', name: 'Reject' },
];



export default class TimeSheet extends LightningElement {
    columns = defaultcolumns
    isManager = false
    selectedTimePeriod;
    submittedTimesheets = []
    timeSheetData = [{
        id: 0
    }, {
        id: 1
    }, {
        id: 2
    }, {
        id: 3
    }, {
        id: 4
    }, {
        id: 5
    }, {
        id: 6
    }]

    connectedCallback() {
        isManager().then(data => {
            if (data) {
                this.columns = managerColumns
                this.isManager = true;
                return;
            }
            // this.columns = this.defaultRows;
            this.columns = defaultcolumns;
        })
        .catch(err => {
            console.log('---err while getting profile info',err)
        })
    }

    get showSubmitButton() {
        return this.isManager ? false : true
    }

    @wire(getTimeSheets)
    recordsList({ data, error }) {
        if (data) {
            const data2 = data.map(item => {
                return { ...item, employee: item.Employee__r.Name, fordate: item.date__c }
            })
            this.submittedTimesheets = data2;
        } else if (error) {
            console.log('------err', error)
        }
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'Reject':
                this.rejectRow(row);
                break;
            case 'Approve':
                this.approveRow(row);
                break;
            default:
        }
    }

    approveRow(row) {
        const { Id } = row;
        this.handleUpdateTimesheet('Approved', Id)
    }

    rejectRow(row) {
        const { Id } = row;
        this.handleUpdateTimesheet('Rejected', Id)
    }

    handleUpdateTimesheet(type, Id) {
        const fields = {};
        fields[ID_FIELD.fieldApiName] = Id;
        fields[STATUS_FIELD.fieldApiName] = type;
        const recordInput = { fields };
        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Contact updated',
                        variant: 'success'
                    })
                );
                return refreshApex(this.recordsList);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating record',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    }

    get getWeeks() {
        const curr = new Date;
        const dates = [];
        for (let i = 0; i < 4; i++) {
            const dt = new Date(curr.setDate(curr.getDate() - curr.getDay() + i * 7)).toLocaleDateString();
            dates.push({ label: dt, value: dt })
        }
        return dates;
    }

    get getDates() {
        if (!this.selectedTimePeriod) {
            return null;
        }
        const curr = new Date(this.selectedTimePeriod);
        const dates = [];
        for (let i = 0; i < 7; i++) {
            const dt = new Date(curr.setDate(curr.getDate() - curr.getDay() + i)).toLocaleDateString();
            dates.push({ label: dt, value: dt, id: i })
        }
        return dates;
    }

    handleChange(event) {
        alreadySubmitted({
            selectedDate: event.detail.value
        }).then(data => {
            if (data) {
                this.selectedTimePeriod = null;
                this.showToastMessage({ title: null, message: "Already submitted, Please choose different date", variant: "error" })
            } else {
                this.selectedTimePeriod = event.detail.value;
            }
        }).catch(err => {
            console.log('----errrrrr', err);
        })
    }

    handleCurrentdateRecs(event) {
        event.preventDefault();
        if (event.detail) {
            this.timeSheetData = this.timeSheetData.map(item => {
                if (item.id === event.detail.id) {
                    return event.detail;
                }
                return item
            })
        }
    }

    handleSubmit(event) {
        event.preventDefault()
        const unfilledDateData = this.timeSheetData.filter(item => {
            if (!item.frm || !item.to) {
                return true
            }
        })
        if (unfilledDateData.length > 0) {
            this.showToastMessage({ title: "error", message: "Please enter all the details", variant: "error" })
            return
        }

        createTimeSheet({ tsItems: JSON.stringify(this.timeSheetData) })
            .then(data => {
                if (data) {
                    this.showToastMessage({ title: "success", message: "Time sheet updated successfully", variant: "success" });
                }
            })
            .catch(err => {
                console.log('----err', err)
            })
    }

    showToastMessage(args) {
        const event = new ShowToastEvent({
            title: args.title,
            message: args.message,
            variant: args.variant
        });
        this.dispatchEvent(event);
    }
}