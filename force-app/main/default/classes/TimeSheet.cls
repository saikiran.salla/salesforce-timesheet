public class TimeSheet {
  public TimeSheet() {
  }

  @AuraEnabled(cacheable=true)
  public static List<TimeSheet__c> getTimeSheets() {
    // Boolean isManager = isManager();
    String dynQuery = 'SELECT Id, Time_From__c, Time_To__c, Date__c, Date_Submitted__c, Comments__c, Employee__c, Employee__r.Name, Status__c from Timesheet__c';
    if (!isManager()) {
        Id currentUserId = UserInfo.getUserId();
      dynQuery += ' WHERE createdbyid =: currentUserId';
    }
    system.debug(';----dynQuery'+dynQuery);
    return Database.query(dynQuery);
  }

  @AuraEnabled
  public static Boolean alreadySubmitted(String selectedDate) {
    if (String.isNotBlank(selectedDate)) {
      List<TimeSheet__c> tsheets = [
        SELECT Id, date__c
        FROM Timesheet__c
        WHERE date__c = :Date.parse(selectedDate)
        LIMIT 1
      ];

      if (!tsheets.isEmpty()) {
        return true;
      }
    }
    return false;
  }

  @AuraEnabled
  public static Boolean isManager() {
    String profileName = [
      SELECT Id, profile.Name
      FROM user
      WHERE id = :UserInfo.getUserId()
    ]
    .profile.Name;
    if (profileName == 'Manager') {
      return true;
    }
    return false;
  }

  @AuraEnabled
  public static Boolean createTimeSheet(String tsItems) {
    String currUserEmail = UserInfo.getUserEmail();
    List<Account> accList = [
      SELECT Id
      FROM Account
      WHERE email__c = :currUserEmail
      LIMIT 1
    ];
    if (!accList.isEmpty()) {
      List<timesheetItem> tsItemsDeserlz = (List<timesheetItem>) JSON.deserialize(
        tsItems,
        List<timesheetItem>.class
      );

      system.debug('----tsItemsDeserlz' + tsItemsDeserlz);

      List<TimeSheet__c> tsList = new List<TimeSheet__c>();
      for (timesheetItem tsitem : tsItemsDeserlz) {
        TimeSheet__c ts = new TimeSheet__c();
        ts.Time_From__c = tsitem.frm;
        ts.Time_To__c = tsitem.to;
        ts.date__c = Date.parse(tsitem.selectedDate);
        ts.Date_Submitted__c = Date.today();
        ts.Comments__c = tsitem.comment;
        ts.Employee__c = accList[0].Id;
        ts.Status__c = 'Submitted';
        tsList.add(ts);
      }

      try {
        if (!tsList.isEmpty())
          insert tsList;
        return true;
      } catch (Exception e) {
        system.debug('Exception ' + e.getCause() + ' ' + e.getMessage());
        return false;
      }
    }

    return false;
  }

  public class timesheetItem {
    public integer id;
    public integer frm;
    public integer to;
    public String selectedDate;
    public string comment;
  }
}